package handlers

import (
	"encoding/json"
	"encoding/xml"
	"net/http"
	"strings"

	mockdb "gitlab.com/ElijahGood/mock_api/mock_db"
	t "gitlab.com/ElijahGood/mock_api/types"
)

func AmountHandler(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "POST":
		format := strings.TrimPrefix(r.URL.Path, "/amount/")
		var mocks t.MockingStruct
		switch format {
		case "json":
			err := json.NewDecoder(r.Body).Decode(&mocks)
			if err != nil {
				http.Error(w, err.Error(), http.StatusBadRequest)
				return
			}
			items := mockdb.GetTotalitem(mocks)
			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(http.StatusOK)
			json.NewEncoder(w).Encode(items)
		case "xml":
			err := xml.NewDecoder(r.Body).Decode(&mocks)
			if err != nil {
				http.Error(w, err.Error(), http.StatusBadRequest)
				return
			}
			items := mockdb.GetTotalitem(mocks)
			w.Header().Set("Content-Type", "application/xml")
			w.WriteHeader(http.StatusOK)
			json.NewEncoder(w).Encode(items)
		default:
			w.WriteHeader(http.StatusNotFound)
			w.Write([]byte(`{"message": "method not found"}`))
		}
	default:
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte(`{"message": "method not found"}`))
	}
}
