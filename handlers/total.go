package handlers

import (
	"encoding/json"
	"net/http"

	mockdb "gitlab.com/ElijahGood/mock_api/mock_db"
	t "gitlab.com/ElijahGood/mock_api/types"
)

func TotalHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	switch r.Method {
	case "POST":
		var mocks t.MockingStruct
		err := json.NewDecoder(r.Body).Decode(&mocks)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
		w.WriteHeader(http.StatusOK)
		items := mockdb.GetTotalitem(mocks)
		json.NewEncoder(w).Encode(items)
	case "GET":
		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(mockdb.GetTotal())
	default:
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte(`{"message": "method not found"}`))
	}
}
