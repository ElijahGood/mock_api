package main

import (
	"log"
	"net/http"

	h "gitlab.com/ElijahGood/mock_api/handlers"
)

func main() {
	http.HandleFunc("/total", h.TotalHandler)
	http.HandleFunc("/amount/", h.AmountHandler)

	log.Fatal(http.ListenAndServe(":8080", nil))
}
