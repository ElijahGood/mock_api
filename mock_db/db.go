package mock_db

import (
	"math/rand"

	t "gitlab.com/ElijahGood/mock_api/types"
)

type Globals struct {
	Data   []t.MockingStruct
	Places []string
}

var (
	global Globals
)

func init() {
	global.Places = []string{"Kyiv", "London", "Paris", "Madrid", "Berlin", "Rome", "Washington", "Dublin", "Amsterdam", "Copenhagen"}
	for i := 0; i < 8; i++ {
		global.Data = append(global.Data, t.MockingStruct{
			Source:      global.Places[rand.Intn(len(global.Places))],
			Destination: global.Places[rand.Intn(len(global.Places))],
			Dimentions:  t.Sizes{rand.Intn(100), rand.Intn(100), rand.Intn(100)},
		})
	}
	global.Data = append(global.Data, t.MockingStruct{
		Source:      global.Places[0],
		Destination: global.Places[8],
		Dimentions:  t.Sizes{42, 42, 42},
	})
	global.Data = append(global.Data, t.MockingStruct{
		Source:      global.Places[0],
		Destination: global.Places[8],
		Dimentions:  t.Sizes{100, 100, 100},
	})
}

func GetTotalitem(req t.MockingStruct) []t.MockingStruct {
	var results []t.MockingStruct
	for _, item := range global.Data {
		if item.Source == req.Source && item.Destination == req.Destination &&
			item.Dimentions.Lenght <= req.Dimentions.Lenght &&
			item.Dimentions.Width <= req.Dimentions.Width &&
			item.Dimentions.Height <= req.Dimentions.Height {
			results = append(results, item)
		}
	}

	return results
}

func GetTotal() []t.MockingStruct {
	return global.Data
}

func GetAmount(req t.MockingStruct) t.MockingStruct {
	var minSized t.MockingStruct = global.Data[0]
	for _, item := range global.Data {
		if item.Volume() < minSized.Volume() {
			minSized = item
		}
	}

	return minSized
}
