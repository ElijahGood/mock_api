package main

import (
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	h "gitlab.com/ElijahGood/mock_api/handlers"
	"gitlab.com/ElijahGood/mock_api/mock_db"
	mock_types "gitlab.com/ElijahGood/mock_api/types"
)

func TestTotalItemOne(t *testing.T) {
	mock := mock_types.MockingStruct{
		Source:      "Kyiv",
		Destination: "Amsterdam",
		Dimentions:  mock_types.Sizes{42, 42, 42},
	}
	res := mock_db.GetTotalitem(mock)
	if res != nil {
		t.Log(res)
	} else {
		t.Errorf("%v IS INVALID", mock)
	}
}

func TestTotalItemTwo(t *testing.T) {
	mock := mock_types.MockingStruct{
		Source:      "Kyiv",
		Destination: "Amsterdam",
		Dimentions:  mock_types.Sizes{150, 150, 150},
	}
	res := mock_db.GetTotalitem(mock)
	if res != nil || len(res) < 1 {
		t.Log(res)
	} else {
		t.Errorf("%v IS VALID", mock)
	}
}

func TestAmountHandler(t *testing.T) {
	url := "http://localhost:8080/amount/xml"

	payload := strings.NewReader("<root>\n   <destination>Kyiv</destination>\n   <dimentions>\n      <height>42</height>\n      <lenght>42</lenght>\n      <width>42</width>\n   </dimentions>\n   <source>Amsterdam</source>\n</root>")

	req, _ := http.NewRequest("POST", url, payload)
	req.Header.Add("content-type", "application/xml")

	handler := http.HandlerFunc(h.AmountHandler)
	resp := httptest.NewRecorder()
	handler.ServeHTTP(resp, req)
	if resp != nil {
		t.Log(resp)
	} else {
		t.Errorf("%v IS INVALID", req)
	}
}

// func TestMain(m *testing.M) {
// 	m.Run()

// 	os.Exit(0)
// }
