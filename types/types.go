package types

type Sizes struct {
	Lenght int `json:"lenght" xml:"lenght"`
	Width  int `json:"width" xml:"width"`
	Height int `json:"height" xml:"height"`
}

// type Coordinates struct {
// 	Latitude, Longitude float32
// }

type MockingStruct struct {
	Source      string `json:"source" xml:"source"`
	Destination string `json:"destination" xml:"destination"`
	Dimentions  Sizes  `json:"dimentions" xml:"dimentions"`
}

func (m MockingStruct) Volume() int {
	return m.Dimentions.Height * m.Dimentions.Lenght * m.Dimentions.Width
}
